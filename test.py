# -*- coding: utf-8 -*
import os  
import time
"""
# [Content] XML Footer Text
def test_hasHomePageNode():
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('首頁') != -1

# [Behavior] Tap the coordinate on the screen
def test_tapSidebar():
	os.system('adb shell input tap 100 100')
"""
# 1. [Content] Side Bar Text
def test_ContentSideBarText():
	os.system('adb shell input tap 100 100')
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	os.system('ren window_dump.xml 1_ContentSideBarText.xml')
	f = open('1_ContentSideBarText.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('查看商品分類') != -1
	assert xmlString.find('查訂單/退訂退款') != -1
	assert xmlString.find('追蹤/買過/看過清單') != -1
	assert xmlString.find('智慧標籤') != -1

	assert xmlString.find('其他') != -1
	assert xmlString.find('PChome 旅遊') != -1
	assert xmlString.find('線上手機回收') != -1
	assert xmlString.find('給24h購物APP評分') != -1	

# 2. [Screenshot] Side Bar Text
def test_ScreenshotSideBarText():
	os.system('adb shell screencap -p /sdcard/screen.png')
	os.system('adb pull /sdcard/screen.png')
	os.system('ren screen.png 2_ScreenshotSideBarText.png')

# 3. [Context] Categories
def test_ContentCategories():
	# go to home
	os.system('adb shell input keyevent 4')
	time.sleep(2)

	# swipe down
	os.system('adb shell input swipe 300 1000 300 100')
	time.sleep(2)

	# tap category
	os.system('adb shell input tap 1000 220')
	time.sleep(2)

	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	os.system('ren window_dump.xml 3_ContentCategories.xml')
	f = open('3_ContentCategories.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('精選') != -1
	assert xmlString.find('3C') != -1
	assert xmlString.find('周邊') != -1
	assert xmlString.find('NB') != -1
	assert xmlString.find('通訊') != -1
	assert xmlString.find('數位') != -1
	assert xmlString.find('家電') != -1
	assert xmlString.find('日用') != -1	
	assert xmlString.find('食品') != -1
	assert xmlString.find('生活') != -1
	assert xmlString.find('運動戶外') != -1
	assert xmlString.find('美妝') != -1
	assert xmlString.find('衣鞋包錶') != -1	

# 4. [Screenshot] Categories
def test_ScreenshotCategories():
	os.system('adb shell screencap -p /sdcard/screen.png')
	os.system('adb pull /sdcard/screen.png')
	os.system('ren screen.png 4_ScreenshotCategories.png')

	# tap category
	os.system('adb shell input tap 1000 220')
	time.sleep(2)


# 5. [Context] Categories page
def test_ContentCategoriesPage():
	os.system('adb shell input tap 100 100')
	os.system('adb shell input tap 100 600')
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	os.system('ren window_dump.xml 5_ContentCategoriesPage.xml')
	f = open('5_ContentCategoriesPage.xml', 'r', encoding="utf-8")
	xmlString = f.read()

	assert xmlString.find('3C') != -1
	assert xmlString.find('周邊') != -1
	assert xmlString.find('NB') != -1
	assert xmlString.find('通訊') != -1
	assert xmlString.find('數位') != -1
	assert xmlString.find('家電') != -1
	assert xmlString.find('日用') != -1	
	assert xmlString.find('食品') != -1
	assert xmlString.find('生活') != -1
	assert xmlString.find('運動戶外') != -1
	assert xmlString.find('美妝') != -1
	assert xmlString.find('衣鞋包錶') != -1
	#assert xmlString.find('書店') != -1
	#assert xmlString.find('電子票券') != -1

# 6. [Screenshot] Categories page
def test_ScreenshotCategoriesPage():
	os.system('adb shell screencap -p /sdcard/screen.png')
	os.system('adb pull /sdcard/screen.png')
	os.system('ren screen.png 6_ScreenshotCategoriesPage.png')

# 7. [Behavior] Search item “switch”
def test_BehaviorSearchItemSwitch():
	os.system('adb shell input tap 100 100')
	time.sleep(3)
	os.system('adb shell input text "switch"')
	time.sleep(2)
	os.system('adb shell input keyevent "KEYCODE_ENTER"')
	time.sleep(3)

# 8. [Behavior] Follow an item and it should be add to the list
def test_BehaviorFollowAnItem():
	# choose switch
	os.system('adb shell input tap 500 900')
	time.sleep(3)	

	# add to like
	os.system('adb shell input tap 100 1700')
	time.sleep(2)

	os.system('adb shell screencap -p /sdcard/screen.png')
	time.sleep(2)
	os.system('adb pull /sdcard/screen.png .')
	os.system('ren screen.png 8_BehaviorFollowAnItem.png')

	# go back
	os.system('adb shell input tap 100 100')
	time.sleep(2)

	# go to home
	os.system('adb shell input tap 100 1700')
	time.sleep(2)

	# go to category
	os.system('adb shell input tap 100 100')
	time.sleep(2)

	# go to like
	os.system('adb shell input tap 100 850')
	time.sleep(5)

# 9. [Behavior] Navigate to the detail of item
def test_BehaviorNavigateDetail():
	# F5
	os.system('adb shell input swipe 500 500 500 1200')
	time.sleep(3)

	# choose switch
	os.system('adb shell input tap 200 850')
	time.sleep(5)
	
	# go detail 
	os.system('adb shell input tap 500 100')
	time.sleep(5)

	os.system('adb shell screencap -p /sdcard/screen.png')
	os.system('adb pull /sdcard/screen.png')
	os.system('ren screen.png 9_BehaviorNavigateDetail.png')
	
# 10. [Screenshot] Disconnetion Screen
def test_ScreenDisconnetion():
	os.system('adb shell svc data disable')
	time.sleep(2)

	os.system('adb shell screencap -p /sdcard/screen.png')
	os.system('adb pull /sdcard/screen.png .')
	os.system('ren screen.png 10_ScreenDisconnetion.png')

